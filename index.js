module.exports = {
  extends: [
    './rules/best-practices',
    './rules/errors',
    './rules/es2017',
  ].map(require.resolve),
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module',
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true,
    },
  },
  rules: {
    strict: 'warn'
  },
};
