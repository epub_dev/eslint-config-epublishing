# ePublishing ESLint Configuration

Copyright (C) 2017, ePublishing, Inc.

## Installation Steps

### 1. Peer Dependencies

First, install this package's peer dependencies and save them to your project's package.json file in `devDependencies`:

```sh
$ cd my_epublishing_project
$ npm install --save-dev eslint eslint-plugin-import eslint-plugin-react
```

#### Optional Alternative

You can simplify this step by using an external utility instead of the command in the above example:

```sh
# This first command only needs to be run a single time. It will install
# a CLI utility called "install-peerdeps" as a global Node module:
$ npm install --global install-peerdeps

# Once you've installed the utility, run it from the root of your project:
$ install-peerdeps --dev @epublishing/eslint-config-epublishing
```

### 2. Install this Package

```sh
$ npm install --save-dev @epublishing/eslint-config-epublishing
```

### 3. Configure ESLint

Create a file named `.eslintrc` in the root of your project and give it the following contents for starters:

```json
{
  "extends": [ "@epublishing/epublishing" ]
}
```

You can then add additional configuration to your `.eslintrc` to override any of the defaults defined by this package. See the [ESLint docs](http://eslint.org/docs/user-guide/configuring) for more information.

## Usage

### Command Line

ESLint will automatically use the project's `.eslintrc` file, so all you have to do in order to lint your scripts in terminal is:

```sh
# Install the eslint CLI utility if necessary:
$ npm install -g eslint

# Lint all .js files in found in app/js against configured rules:
$ eslint app/js/**/*.js
```

### Editor Integration

Plugins that provide inline error reporting are available for most editors and IDEs:

+ [Atom](https://atom.io/packages/linter-eslint)
+ [VS Code](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
+ [Sublime Text](https://packagecontrol.io/packages/ESLint)
+ [Vim](https://github.com/vim-syntastic/syntastic)

## Release Workflow

This repository includes `bumped` as a development dependency and defines a few hooks to simplify the process of publishing a new release to Bitbucket and the NPM registry.

```sh
npm run release $TYPE
```

Where `$TYPE` can be any of the release arguments accepted by the `bumped release` command.

### Examples

_Fix a bug and publish a new patch release:_

```sh
$ npm run release patch

  > @epublishing/eslint-config-epublishing@0.1.1 release /home/mike/development/work/eslint-config-epublishing
  > bumped release "patch"


  bumped Releases version 0.1.2.

  post bumped Starting Publishing tag to Bitbucket
  post bumped Everything up-to-date
  post bumped To bitbucket.org:epub_dev/eslint-config-epublishing.git
   * [new tag]         v0.1.2 -> v0.1.2
  post bumped Finished Publishing tag to GitHub after 1.9s.

  post bumped Starting Publishing to NPM
  post bumped + @epublishing/eslint-config-epublishing@0.1.2
  post bumped Finished Publishing to NPM after 3.9s.
```
