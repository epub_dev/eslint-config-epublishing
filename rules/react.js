/**
 * React ESLint rules
 */

'use strict';

module.exports = {
  extends: [ 'plugin:react/recommended' ],
  plugins: [ 'react' ],
  rules: {
    'react/no-danger': 'warn',
    'react/prop-types': 'warn',
  }
};
