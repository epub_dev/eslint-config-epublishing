/**
 * Rules to catch basic syntax mistakes
 *
 * @see {@link https://github.com/airbnb/javascript/blob/master/packages/eslint-config-airbnb-base/rules/errors.js}
 */

'use strict';

module.exports = {
  rules: {
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'ignore',
    }],
    'no-await-in-loop': 'error',
    'no-compare-neg-zero': 'off',
    'no-cond-assign': [ 'error', 'always' ],
    'no-console': 'warn',
    'no-constant-condition': 'warn',
    'no-control-regex': 'warn',
    'no-debugger': 'error',
    'no-dupe-args': 'error',
    'no-dupe-keys': 'error',
    'no-duplicate-case': 'error',
    'no-empty': 'error',
    'no-empty-character-class': 'error',
    'no-ex-assign': 'error',
    'no-extra-boolean-cast': 'error',
    'no-extra-parens': ['off', 'all', {
      conditionalAssign: true,
      nestedBinaryExpressions: false,
      returnAssign: false,
    }],
    'no-extra-semi': 'error',
    'no-func-assign': 'error',
    'no-inner-declarations': 'warn',
    'no-obj-calls': 'error',
    'no-prototype-builtins': 'error',
    'no-regex-spaces': 'error',
    'no-sparse-arrays': 'error',
    'no-template-curly-in-string': 'error',
    'no-unexpected-multiline': 'error',
    'no-unsafe-finally': 'error',
    'no-unsafe-negation': 'error',
    'no-negated-in-lhs': 'off',
    'use-isnan': 'error',
    'valid-jsdoc': ['warn', {
      prefer: {
        return: 'returns',
        arg: 'param',
        argument: 'param',
      },
      preferType: {
        object: 'Object',
        String: 'string',
        Number: 'number',
        Boolean: 'boolean',
      },
      requireReturn: false,
    }],
    'valid-typeof': [ 'error', { requireStringLiterals: true } ],
  },
};
