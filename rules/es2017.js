/**
 * Next Generation ECMAScript Rules
 */

'use strict';

module.exports = {
  rules: {
    'arrow-body-style': [ 'error', 'as-needed', { requireReturnForObjectLiteral: false } ],
    'arrow-parens': [ 'error', 'always' ],
    'arrow-spacing': [ 'error', { before: true, after: true } ],
    'constructor-super': 'error',
    'generator-star-spacing': [ 'error', { before: false, after: true } ],
    'no-class-assign': 'error',
    'no-confusing-arrow': [ 'error', { allowParens: true } ],
    'no-const-assign': 'error',
    'no-dupe-class-members': 'error',
    'no-duplicate-imports': 'error',
    'no-new-symbol': 'error',
    'no-restricted-imports': 'off',
    'no-this-before-super': 'error',
    'no-useless-computed-key': 'error',
    'no-useless-constructor': 'error',
    'no-useless-rename': [ 'error', {
      ignoreDestructuring: false,
      ignoreImport: false,
      ignoreExport: false,
    }],
    'no-var': 'warn',
    'object-shorthand': [ 'warn', 'always', {
      ignoreConstructors: false,
      avoidQuotes: true,
    }],
    'prefer-const': [ 'warn', {
      destructuring: 'any',
      ignoreReadBeforeAssign: true,
    }],
    'prefer-numeric-literals': 'off',
    'prefer-rest-params': 'error',
    'prefer-spread': 'error',
    'prefer-template': 'error',
    'require-yield': 'error',
    'symbol-description': 'error',
    'template-curly-spacing': 'error',
    'yield-star-spacing': [ 'error', 'after' ],
  },
};
